### Version ###

* 1.0.1


### Fix ###

###### v 1.0.1
* build other js fix
* inject other js fix


### How to install and use ###

* Download and install [nodejs](https://nodejs.org/)
* Download and install [git](https://git-scm.com/)
* Create a project folder
* Clone a branch with a gulp template

| Type | Code |
| ------ | ------ |
| https | ```git clone https://devrom@bitbucket.org/devrom/gulp-markup-template.git``` |
| ssh | ```git clone git@bitbucket.org:devrom/gulp-markup-template.git``` |

* Install the GULP globally
```
npm i gulp -g
```

* Open the project folder in the terminal and install the necessary modules
```
npm i
```



### Recommendations for setting up a code editor ###

I recommend using the code editor [sublimetext3](https://www.sublimetext.com/3)
Install the plugin Control Packedge
```
import urllib.request,os,hashlib; h = 'df21e130d211cfc94d9b0905775a7c0f' + '1e3d39e33b79698005270310898eea76'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
```

Install the Gulp plugin in the editor
* Use: CTRL + SHIFT + P
* Find: Install Packedge
* Enter
* Find: Gulp
* Enter
* Wait for the plugin installation
* Restart the sublimetext3



### Guided by tasks gulp ###

| Task | Description |
| ------ | ------ |
| ```removedist``` | Remove the dist directory |
| ```clearcache``` | Clearing the cache of gulp |
| ```browser-sync``` | Sync with browser |
| ```custom-js``` | Combines all js files from the folder with js/custom into one file with the name common.min.js |
| ```libs-js``` | Combines all js files from the folder with libs/name_of_lib/dist into one file with the name libs.min.js |
| ```head-sass``` | Compiles head.sass to head.min.css |
| ```sass``` | Combines fonts.sass, main.sass, media.sass, libs.sass files into one file with the name main.min.css |
| ```inject``` | Include styles and scripts to html template |
| ```fileinclude``` | Compiles all html template except header.html, header_subname.html, footer.html, footer_subname.html |
| ```watch``` | Prints the result to the browser |
| ```getWork``` | The main task. Used for development |
| ```build-minify``` | Compiles the project folder with compressed and minified files |
| ```get-files-build-minify``` | Compiles the project folder with compressed css, js, img |
| ```move-html-to-dist``` | Moves html from folder app to folder dist |
| ```build-unminify``` | Compiles the project folder with normal files (not compressed files) |
| ```get-files-build-unminify``` | Compiles the project folder with compressed img and normal css, js |
| ```inject-build-unminify``` | Include css and js to html template |


### Guide to folders and project files ###

| Folder/file name | Description |
| ------ | ------ |
| fonts | Folder with custom fonts |
| html | Folder for html templates |
| img | Image directory |
| js | Directory for custom js |
| libs | Folder contains the js libraries |
| sass | Folder contains sass files |

### Who do I talk to? ###

* Admin mail: rjdesua@gmail.com
* Admin skype: b1rtooos

### LICENSE ###

MIT